package util;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBUtil {

    //ENCAPSULATION IS DONE HERE TO RESTRICT THE DATA TO BE CHANGED AND ACCESS DIRECTLY (hiding sensitive data)
    private static String url = ConfigReader.getProperty("DBUrl");
    private static String username = ConfigReader.getProperty("DBUsername");
    private static String password = ConfigReader.getProperty("DBPassword");

    private static Connection connection; // use to connect to database
    private static Statement statement; // use to send the query to database
    private static ResultSet resultSet; // gets the results return from the query

    public static Connection createDBConnection(){
        try {
            connection = DriverManager.getConnection(url, username, password); //CREATING THE CONNECTION TO THE DATABASE WITH THE PARAMETERS
            System.out.println("Database connection is successful");
        }catch (SQLException e){
            System.out.println("Database connection failed");
            e.printStackTrace();
        }
        return connection;
    }

    public static void executeQuery(String query){
        try{
            statement = connection.createStatement(); // STATEMENT KEEPS THE CONNECTION BETWEEN PROGRAM AND THE DATA SOURCE(DATABASE) FOR SENDING THE SQL QUERIES
            resultSet = statement.executeQuery(query); // here is where the query we want is passed in to be executed
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public static List<List<Object>> getQueryResultList(String query){
        executeQuery(query);
        List<List<Object>> rowList = new ArrayList<>(); // container to hold the result(list of list)

        // WE NEED TO FIND NUMBER OF COLUMN TO STOP OUR LOOP AT THE LIMIT
        ResultSetMetaData resultSetMetaData;

        try {
            resultSetMetaData=resultSet.getMetaData(); // this code is returning the table information
            while (resultSet.next()){// this is looping through each row; while table has next row keep looping
                List<Object> row = new ArrayList<>(); // inner container to hold the result(list)
                for (int i=1; i <= resultSetMetaData.getColumnCount(); i++){ // 2 columns; resultSetMetaData.getColumnCount() --> this giving us the number of column in the table
                    row.add(resultSet.getObject(i)); // adding value selected from the query defined in TESTDBUtil to small list
                }
                rowList.add(row); // adding small list into big list
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return rowList;
    }

    public static Object getCellValue(String query) {
        // if we have only one value from one query we use this method because we don't need lists of list.
        return getQueryResultList(query).get(0).get(0);
    }

}
