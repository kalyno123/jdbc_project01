package stepDef;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import util.DBUtil;

import java.util.ArrayList;
import java.util.List;

public class DatabaseStepDef {
    String classQuery;
    List<List<Object>> actualLists = new ArrayList<>();

    @Given("user is able to connect to database")
    public void userIsAbleToConnectToDatabase() {
        DBUtil.createDBConnection();
    }

    @When("User send the {string} to database")
    public void user_send_the_to_database(String query) {
        classQuery = query;
        //DBUtil.executeQuery(query);
        actualLists = DBUtil.getQueryResultList(query);
        System.out.println("Query execution is successful");
    }

    @Then("Validate the employee's first name and last name displayed")
    public void validateTheEmployeeSFirstNameAndLastNameDisplayed(DataTable resultTable) {
        for (int i = 0; i < resultTable.asLists().size(); i++) {
            Assert.assertEquals(actualLists.get(i), resultTable.asLists().get(i)); // (actual, expected)
            //Assert.assertEquals(DBUtil.getQueryResultList(query).get(i), resultTable.asLists().get(i)); // (actual, expected)
            //System.out.println(rowList.get(i));
            //System.out.println(resultTable.asLists().get(i));
        }
    }

    // JBDC IS JUST A LIBRARY WHICH IS USED TO CONNECT TO THE DATABASE WITH THE HELP OF 3 INTERFACES: CONNECTION, STATEMENT, AND RESULTSET.

}
