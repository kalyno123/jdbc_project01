Feature: As a QE, I validate that I can connect to DB

  @db
  Scenario: Validating the Database Connection
    Given user is able to connect to database
    When User send the "select first_name, last_name from employees where manager_id = (select manager_id from employees where first_name='Payam')" to database
    Then Validate the employee's first name and last name displayed
      | Neena   | Kochhar   |
      | Lex     | De Haan   |
      | Den     | Raphaely  |
      | Matthew | Weiss     |
      | Adam    | Fripp     |
      | Payam   | Kaufling  |
      | Shanta  | Vollman   |
      | Kevin   | Mourgos   |
      | John    | Russell   |
      | Karen   | Partners  |
      | Alberto | Errazuriz |
      | Gerald  | Cambrault |
      | Eleni   | Zlotkey   |
      | Michael | Hartstein |